<?php

return [
    'email_verification_expiry_time' => env('EMAIL_VERIFICATION_EXPIRY_TIME', 300),
    'reset_password_expiry_time' => env('RESET_PASSWORD_EXPIRY_TIME', 300),
    'api_version' => env('API_VERSION', 'api/v1'),
    

];
