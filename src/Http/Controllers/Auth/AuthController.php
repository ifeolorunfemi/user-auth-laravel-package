<?php


namespace ForteA\User\Http\Controllers\Auth;


use ForteA\User\Exceptions\ValidationErrorRequest;
use ForteA\User\Http\Controllers\Controller;
use ForteA\User\Http\Requests\ForgotPasswordRequest;
use ForteA\User\Http\Requests\LoginRequest;
use ForteA\User\Http\Requests\RegisterRequest;
use ForteA\User\Http\Requests\ResetPassword;
use ForteA\User\Models\PasswordReset;
use ForteA\User\Models\User;
use ForteA\User\Notifications\PasswordResetRequest;
use ForteA\User\Notifications\PasswordResetSuccess;
use Illuminate\Auth\Events\PasswordReset as EventsPasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Str;


class AuthController extends Controller
{
    //

    public function login(LoginRequest $request)
    {
        $data = $request->validated();
        if (!auth()->attempt($data))
            return response()->error('Invalid Email or Password', null, 401);

        $user = auth()->user();

        $accessToken = auth()->user()->createToken($_SERVER['HTTP_USER_AGENT'])->plainTextToken;


        return response()->success('Logged in successfully.', [$user, 'token'   => $accessToken]);
    }

    public function logout()
    {
        auth()->user()->currentAccessToken()->delete();

        return response()->success('Logout Successful');
    }

    public function register(RegisterRequest $request)
    {
        try
        {
            $data = $request->validated();
            $user = User::create($data);
            $user->roles()->attach(2);
            return response()->success('Created', $user, 201);
        }
        catch (\Throwable $th)
        {
            throw new ValidationErrorRequest($th->getMessage());
        }
    }

    public function me()
    {
        $data = auth()->user();
        return response()->success('Retrieved', $data, 200);
    }

    public function forgotPassword(ForgotPasswordRequest $request)
    {
        $data  =  $request->validated();
        $model = User::whereEmail($data['email'])->get()->first();

        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $model->email],
            [
                'email' => $model->email,
                'token' => Str::random(60)
            ]
        );

        $passwordReset = PasswordReset::whereEmail($model->email)->first();
        if ($passwordReset)
        {
            $passwordReset->token = random_int(100000, 999999);
            $passwordReset->update();
        }
        else
        {
            $passwordReset = PasswordReset::updateOrCreate([
                'email' => $model->email,
                'token' => random_int(100000, 999999)
            ]);
        }

        if ($model && $passwordReset)
        {
            try
            {
                /*   $model->notify(
                    new PasswordResetRequest($passwordReset->token)
                ); */
                event(new EventsPasswordReset($model));
            }
            catch (\Throwable $th)
            {
                //throw $th;
                throw new ValidationErrorRequest($th->getMessage());
            }
        }
        return response()->success('Password reset token has been sent to your email.');
    }


    public function find($token)
    {
        $passwordReset = PasswordReset::where('token', $token)
            ->get()
            ->first();
        if (!$passwordReset)
            return response()->error('Password reset token is invalid.');
        if (Date::parse($passwordReset->updated_at)->addMinutes(config('user.reset_password_expiry_time'))->isPast())
        {
            $passwordReset->delete();
            return response()->error('Password reset token is invalid.');
        }
        return response()->success('Retrieved Successfully', $passwordReset);
    }

    public function reset(ResetPassword $request)
    {
        $data = $request->validated();
        $passwordReset = PasswordReset::where([
            ['token', $data['token']]
        ])->get()->first();
        if (!$passwordReset)
            return response()->error('Token expired.');
        $user = User::where('email', $passwordReset->email)->first();
        if (!$user)
            return response()->error('We can\'t find a user with that e-mail address.');
        $user->password = bcrypt($data['password']);
        $user->update();
        $passwordReset->delete();
        try
        {
            $user->notify((new PasswordResetSuccess($passwordReset))->delay(now()->addSecond(30)));
        }
        catch (\Throwable $th)
        {
            //throw $th;
            throw new ValidationErrorRequest($th->getMessage());
        }
        return response()->success('Your password has been successfully changed.');
    }
}
