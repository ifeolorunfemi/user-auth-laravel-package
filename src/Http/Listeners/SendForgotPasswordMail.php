<?php

namespace ForteA\User\Http\Listeners;

use ForteA\User\Http\Events\ForgotPasswordEvent;
use ForteA\User\Models\PasswordReset;
use ForteA\User\Notifications\PasswordResetRequest;
use Illuminate\Auth\Events\PasswordReset as EventsPasswordReset;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendForgotPasswordMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ForgotPasswordEvent  $event
     * @return void
     */
    public function handle(EventsPasswordReset $event)
    {
        //
        $passwordReset = PasswordReset::whereEmail($event->user->email)->get()->first();
        $event->user->notify(
            (new PasswordResetRequest($passwordReset->token))->delay(now()->addSecond(5))
        );
    }
}
