<?php

namespace ForteA\User\Http\Requests;


use App\Exceptions\ValidationErrorRequest;
use ForteA\User\Exceptions\ValidationErrorRequest as ExceptionsValidationErrorRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class ForgotPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new ExceptionsValidationErrorRequest($validator->errors());
    }
}
