<?php


namespace ForteA\User\Http\Middleware;


use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class checkIfAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $userRoles = Auth::user()->roles->pluck('name');

        if (!$userRoles->contains('admin'))
        {
            return response(['status' => 'FAILED', 'message' => 'You do not have the permission to perform this operation.'], 403);
        }

        return $next($request);
    }
}
