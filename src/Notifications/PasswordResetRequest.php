<?php

namespace ForteA\User\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PasswordResetRequest extends Notification implements ShouldQueue
{
    use Queueable;


    protected $token;

    protected $newAccount;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token, $newAccount = false)
    {
        $this->token = $token;

        $this->newAccount = $newAccount;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = config('app.frontend_url') . '/password/reset?token=' . $this->token;

        if ($this->newAccount)
        {
            return (new MailMessage)
                ->subject('New Account')
                ->line('A new account has been created for you on ' . config('app.name') . '. Click the button below to set your password.')
                ->action('Create Password', $url);
        }

        $tokenExpiryToMinutes = config('user.reset_password_expiry_time') / 60;
        return (new MailMessage)
            ->subject('Reset Your Password')
            ->line('Kindly use the code below to reset your password. ')
            ->line('Your verification code is '  . $this->token)
            ->line('The code will expire in ' . $tokenExpiryToMinutes . ' minutes.')
            ->line('Please ignore this email if you did not initiate this.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'token' => $this->token,
        ];
    }
}
