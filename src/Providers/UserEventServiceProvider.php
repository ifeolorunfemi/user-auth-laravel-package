<?php

namespace ForteA\User\Providers;

use ForteA\User\Http\Listeners\SendForgotPasswordMail;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class UserEventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        PasswordReset::class => [
            SendForgotPasswordMail::class,
        ],
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
   

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
