<?php

namespace ForteA\User\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class UserRouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'ForteA\User\Http\Controllers';
    /**
     * Register services.
     *
     * @return void
     */


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        parent::boot();
    }


    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
    }

    /**
     * Define the "user" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix(config('user.api_version'))
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(__DIR__ . '/../routes/api.php');
    }



}
