<?php

namespace ForteA\User\Providers;

use ForteA\User\Http\Middleware\checkIfAdmin;
use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        //
        $this->app->register(UserRouteServiceProvider::class);
        $this->app->register(UserEventServiceProvider::class);

        app('router')->aliasMiddleware('admin', \ForteA\User\Http\Middleware\checkIfAdmin::class);

        $this->publishes([
            __DIR__ . '/../config/config.php' => config_path('user.php'),
        ], 'config');


        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
    }
}
