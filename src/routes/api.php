<?php

use Illuminate\Support\Facades\Route;

Route::group([
// 'namespace' => 'Auth',
'prefix' => 'auth'
], function ($router)
{

Route::post('login', 'Auth\AuthController@login');
Route::post('logout', 'Auth\AuthController@logout')->middleware('auth:sanctum');;
Route::get('me', 'Auth\AuthController@me')->middleware('auth:sanctum');
Route::post('register', 'Auth\AuthController@register');
Route::post('forgot-password', 'Auth\AuthController@forgotPassword');
Route::post('reset-password', 'Auth\AuthController@reset');
Route::get('find/{token}', 'Auth\AuthController@find');
Route::post('reset', 'Auth\AuthController@reset');

});
