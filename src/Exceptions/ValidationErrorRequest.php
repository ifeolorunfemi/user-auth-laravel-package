<?php

namespace ForteA\User\Exceptions;

use Exception;

class ValidationErrorRequest extends Exception
{
    //
    public function __construct($message)
    {
        $this->message = $message;
    }


    public function render($request)
    {
        return response()->error(
            'FAILED',
            $this->message,
         400);
    }
}
