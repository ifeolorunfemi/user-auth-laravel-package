<?php

namespace ForteA\User\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $fillables = ['name'];
    public function users()
    {
        return $this->belongsToMany(User::class, 'users_roles');
    }


}
